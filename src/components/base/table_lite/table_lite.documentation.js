import * as description from './table_lite.md';

export default {
  description,
  bootstrapComponent: 'b-table-lite',
};
