import * as description from './toggle.md';

export default {
  description,
  followsDesignSystem: true,
};
