import * as description from './form_combobox.md';

export default {
  followsDesignSystem: true,
  description,
};
