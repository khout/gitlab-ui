import description from './intersperse.md';

export default {
  followsDesignSystem: false,
  description,
};
